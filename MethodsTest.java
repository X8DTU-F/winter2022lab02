public class MethodsTest {
    public static void main(String[] args) {
        int x = 5;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);
        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x + 50);
        methodTwoInputNoReturn(x, x);
        int l = methodNoInputReturnInt();
        System.out.println(l);
        double b = sumSquareRoot(6, 3);
        System.out.println(b);
        String s1 = "hello";
        String s2 = "goodbye";
        System.out.println(s1.length());
        System.out.println(s2.length());
        int c = SecondClass.addOne(50);
        System.out.println(c);
        SecondClass sc = new SecondClass();
        int d = sc.addTwo(50);
        System.out.println(d);
        
    }
    public static void methodNoInputNoReturn() {
        int x = 50;
        System.out.println(x);
        System.out.println("I'm in a method that takes no input and returns nothing");
    }
    public static void methodOneInputNoReturn(int fun) {
        System.out.println("Inside the method one input no return");
        System.out.println(fun);
    }
    public static void methodTwoInputNoReturn(int x, double y) {
        System.out.println(x);
        System.out.println(y);
    }
    public static int methodNoInputReturnInt() {
        return 6;
    }
    public static double sumSquareRoot(int number1, int number2) {
        int number3 = number1 + number2;
        double sqrt = Math.sqrt(number3);
        return sqrt;
    }
}