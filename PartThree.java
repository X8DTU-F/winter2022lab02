import java.util.Scanner;

public class PartThree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("What is length of the side of the square?");
        double sLength = sc.nextDouble();
        System.out.println("What is length of the side of the rectangle?");
        double rLength = sc.nextDouble();
        System.out.println("What is length of the side of the square?");
        double rWidth = sc.nextDouble();

        double sArea = AreaComputations.areaSquare(sLength);
        AreaComputations compute = new AreaComputations();
        double rArea = compute.areaRectangle(rLength, rWidth);
        System.out.println(sArea);
        System.out.println(rArea);

    }
}